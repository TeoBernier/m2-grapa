from os import listdir
from sys import stdout
import subprocess
import signal

process = []

def exit_gracefully(*args):
    for p in process:
        p.terminate()
    exit(0)

signal.signal(signal.SIGINT, exit_gracefully)

files = listdir('./ressources')

process.extend(subprocess.Popen(f"cargo run --release -- {i} ./ressources/{file}", stdout=stdout) for i, file in enumerate(files))

for p in process:
    p.wait()
