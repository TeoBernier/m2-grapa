use crate::graph::vertex::{Doi, Vertex};
use crate::inner::*;
use pyo3::exceptions::PyException;
use pyo3::prelude::*;
use pyo3::types::{PyList, PyString, PyTuple};
use pyo3::IntoPy;

#[pyclass]
#[derive(Clone)]
pub struct ExtractGraph(Graph);

#[pymethods]
impl ExtractGraph {
    #[new]
    fn new() -> Self {
        ExtractGraph(Graph::new())
    }

    fn __len__(slf: PyRef<'_, Self>) -> usize {
        slf.0.vertices.len()
    }

    fn __contains__(slf: PyRef<'_, Self>, doi: &str) -> bool {
        let doi = Doi::leak_id(doi.to_string());
        let res = slf.0.dois.contains_key(&doi);
        doi.drop_string();
        res
    }

    fn __getitem__(slf: PyRef<'_, Self>, doi: &str) -> PyObject {
        let doi = Doi::leak_id(doi.to_string());
        let res = if let Some(&idx) = slf.0.dois.get(&doi) {
            let Vertex {
                ref_by, ref refs, ..
            } = slf.0.vertices[idx as usize];
            let outgoing = refs[..ref_by as usize]
                .iter()
                .map(|&id| PyString::new(slf.py(), slf.0.vertices[id as usize].doi.id));
            let incoming = refs[ref_by as usize..]
                .iter()
                .map(|&id| PyString::new(slf.py(), slf.0.vertices[id as usize].doi.id));

            PyTuple::new(
                slf.py(),
                [
                    PyList::new(slf.py(), incoming),
                    PyList::new(slf.py(), outgoing),
                ],
            )
            .into_py(slf.py())
        } else {
            slf.py().None()
        };
        doi.drop_string();
        res
    }

    fn combine(mut slf: PyRefMut<'_, Self>, other: PyRef<'_, Self>) {
        slf.0.combine(&other.0)
    }

    #[staticmethod]
    fn load_from_bc(path: &str) -> PyResult<ExtractGraph> {
        match Graph::load_from_bc(path) {
            Ok(graph) => Ok(ExtractGraph(graph)),
            Err(err) => Err(PyException::new_err(format!("{:?}", err))),
        }
    }

    #[staticmethod]
    fn load_from_json(path: &str) -> PyResult<ExtractGraph> {
        match Graph::load_from_json(path) {
            Ok(graph) => Ok(ExtractGraph(graph)),
            Err(err) => Err(PyException::new_err(format!("{:?}", err))),
        }
    }

    #[staticmethod]
    fn load_from_json_gz(path: &str) -> PyResult<ExtractGraph> {
        match Graph::load_from_json_gz(path) {
            Ok(graph) => Ok(ExtractGraph(graph)),
            Err(err) => Err(PyException::new_err(format!("{:?}", err))),
        }
    }

    fn save_to_bc(slf: PyRef<'_, Self>, path: &str) -> PyResult<()> {
        slf.0
            .save_to_bc(path)
            .map_err(|err| PyException::new_err(format!("{:?}", err)))
    }

    fn save_to_json(slf: PyRef<'_, Self>, path: &str) -> PyResult<()> {
        slf.0
            .save_to_json(path)
            .map_err(|err| PyException::new_err(format!("{:?}", err)))
    }
}
