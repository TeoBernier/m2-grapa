use crate::inner::*;
use std::mem::take;

pub struct GraphWideSearch<'a> {
    graph: &'a Graph,
    visited: Vec<bool>,
    working_list: Vec<u32>,
    next_working_list: Vec<u32>,
}

impl<'a> Iterator for GraphWideSearch<'a> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(idx) = self.working_list.pop() {
            self.visited[idx as usize] = true;
            for &new_idx in &self.graph.vertices[idx as usize].refs {
                if !self.visited[new_idx as usize] {
                    self.visited[new_idx as usize] = true;
                    self.next_working_list.push(new_idx);
                }
            }
            Some(idx)
        } else if self.next_working_list.len() > 0 {
            self.working_list = take(&mut self.next_working_list);
            self.next()
        } else {
            None
        }
    }
}

impl<'a> GraphWideSearch<'a> {
    pub fn new(graph: &'a Graph, root: u32) -> Self {
        Self {
            graph,
            visited: vec![false; graph.vertices.len()],
            working_list: vec![root],
            next_working_list: vec![],
        }
    }
}
