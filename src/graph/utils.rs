use std::fs::File;
use std::io::{self, BufReader, BufWriter};
use std::path::Path;

use crate::inner::*;
use bincode::{deserialize_from, serialize_into};
use flate2::bufread::GzDecoder;
use serde_json::{Deserializer, Serializer};

use LoadingError::*;

#[derive(Debug)]
pub enum LoadingError {
    JsonError(serde_json::Error),
    BincodeError(bincode::Error),
    IOError(io::Error),
}

pub type LoadingResult<T> = Result<T, LoadingError>;

impl Graph {
    pub fn load_from_bc<P>(path: P) -> LoadingResult<Graph>
    where
        P: AsRef<Path>,
    {
        deserialize_from(BufReader::new(
            File::open(path).map_err(|err| IOError(err))?,
        ))
        .map_err(|err| BincodeError(err))
    }

    pub fn load_from_json<P>(path: P) -> LoadingResult<Graph>
    where
        P: AsRef<Path>,
    {
        let mut deserializer = Deserializer::from_reader(BufReader::new(
            File::open(path).map_err(|err| IOError(err))?,
        ));

        Graph::deserialize(&mut deserializer).map_err(|err| JsonError(err))
    }

    pub fn load_from_json_gz<P>(path: P) -> LoadingResult<Graph>
    where
        P: AsRef<Path>,
    {
        let mut graph = Graph::new();

        let deserializer = Deserializer::from_reader(GzDecoder::new(BufReader::new(
            File::open(path).map_err(|err| IOError(err))?,
        )));

        for item in deserializer.into_iter::<Item>().filter_map(Result::ok) {
            if item.reference.is_some() {
                graph.insert_item(item);
            }
        }

        Ok(graph)
    }

    pub fn save_to_bc<P>(&self, path: P) -> LoadingResult<()>
    where
        P: AsRef<Path>,
    {
        serialize_into(
            BufWriter::new(File::create(path).map_err(|err| IOError(err))?),
            self,
        )
        .map_err(|err| BincodeError(err))
    }

    pub fn save_graph_to_json<P>(&self, path: P) -> LoadingResult<()>
    where
        P: AsRef<Path>,
    {
        let mut serializer = Serializer::new(BufWriter::new(
            File::create(path).map_err(|err| IOError(err))?,
        ));

        self.edges
            .serialize(&mut serializer)
            .map_err(|err| JsonError(err))
    }

    pub fn save_map_to_json<P>(&self, path: P) -> LoadingResult<()>
    where
        P: AsRef<Path>,
    {
        let mut serializer = Serializer::new(BufWriter::new(
            File::create(path).map_err(|err| IOError(err))?,
        ));

        self.vertices
            .serialize(&mut serializer)
            .map_err(|err| JsonError(err))
    }
}
