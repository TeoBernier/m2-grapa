use extract::prelude::*;

fn main() {
    // read_json_create_graph_and_serialize();
    // serialize_bincode_json();
}

#[allow(unused)]
fn serialize_bincode_json() {
    use std::fs::read_dir;

    let mut files = read_dir("./tmp/")
        .expect("No directory")
        .filter_map(Result::ok)
        .map(|path| path.path());
    let file = files.next().unwrap();
    println!("Processing : {file:?} ...");
    let mut graph = Graph::load_from_bc(file).unwrap();

    for file in files {
        println!("Processing : {file:?} ...");
        graph.combine(&Graph::load_from_bc(&file).unwrap());
    }

    graph.save_to_bc("./results/graph.bc").unwrap();
}

#[allow(unused)]
fn read_json_create_graph_and_serialize() {
    use flate2::bufread::GzDecoder;
    use serde_json::Deserializer;
    use std::env::args;
    use std::fs::{read_dir, File};
    use std::io::{stdout, BufReader, BufWriter, Write};

    let mut args = args().skip(1);

    let num = args.next();

    let args = args.collect::<Vec<_>>();

    let files = if args.len() == 0 {
        read_dir("./ressources")
            .expect("No directory")
            .filter_map(Result::ok)
            .map(|path| path.path().to_str().unwrap().to_string())
            .collect::<Vec<_>>()
    } else {
        args
    };

    let mut graph = Graph::new();

    for file in files {
        println!("Processing {:?}...", file);
        let deserializer = Deserializer::from_reader(GzDecoder::new(BufReader::new(
            File::open(&file).expect("Cannot open file..."),
        )));

        // print!("\rprocessing {:?} | {:>7}", &file, graph.vertices.len());
        stdout().flush().unwrap();

        for item in deserializer.into_iter::<Item>().filter_map(Result::ok) {
            if item.reference.is_some() {
                graph.insert_item(item);
            }
        }
    }

    let path = if let Some(num) = num {
        format!("./tmp/graph_{}.bc", num)
    } else {
        "./results/graph.bc".to_string()
    };

    bincode::serialize_into(
        BufWriter::new(File::create(path).expect("Cannot open file...")),
        &graph,
    )
    .unwrap();
}
