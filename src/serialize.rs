use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Ref {
    #[serde(rename = "DOI")]
    pub doi: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Item {
    #[serde(rename = "DOI")]
    pub doi: String,
    pub reference: Option<Vec<Ref>>,
}
