use crate::inner::*;

pub mod py_api;
pub mod search;
pub mod utils;
mod vertex;

pub use search::*;
pub use utils::*;
use vertex::*;

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Graph {
    edges: Vec<(u16, Vec<u32>)>,
    vertices: HashMap<Box<str>, u32>,
}

impl Graph {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn combine(&mut self, other: &Self) {
        for idx in 0..other.vertices.len() as u32 {
            self.add_vertex(other, idx)
        }
    }

    pub fn add_vertex(&mut self, other: &Self, idx: u32) {
        let Vertex { doi, ref_by, refs } = &other.vertices[idx as usize];
        let idx = self.get_or_add_key(doi.id.to_string());

        let mut new_refs = Vec::with_capacity(refs.len() + self.vertices[idx].refs.len());

        for ref_idx in refs.iter() {
            let other_doi = other.vertices[*ref_idx as usize].doi;
            new_refs.push(self.get_or_add_key(other_doi.id.to_string()) as u32);
        }

        if *ref_by > 0 {
            assert_eq!(self.vertices[idx].ref_by, 0);
            self.push_outgoing_refs(idx, new_refs);
            self.vertices[idx].ref_by = *ref_by;
        } else {
            self.vertices[idx].refs.append(&mut new_refs);
        }
    }

    pub fn wide_search(&self, root: u32) -> GraphWideSearch {
        GraphWideSearch::new(self, root)
    }

    pub fn insert_item(&mut self, item: Item) {
        if let Some(refs) = item.reference {
            let idx = self.get_or_add_key(item.doi);
            self.push_refs(idx, refs);
        }
    }
}

impl Graph {
    fn get_or_add_key(&mut self, id: String) -> usize {
        use std::collections::hash_map::Entry::*;

        let doi = Doi::leak_id(id);

        match self.dois.entry(doi) {
            Vacant(entry) => {
                let new_idx = self.vertices.len();
                entry.insert(new_idx as u32);
                self.vertices.push(Vertex::new(doi));
                new_idx
            }
            Occupied(entry) => {
                unsafe { drop(Box::from_raw(doi.id as *const str as *mut str)) };
                *entry.get() as usize
            }
        }
    }

    fn push_outgoing_refs(&mut self, idx: usize, mut outgoing_refs: Vec<u32>) {
        let Vertex { refs, .. } = &mut self.vertices[idx];
        outgoing_refs.append(refs);
        *refs = outgoing_refs;
    }

    fn push_refs(&mut self, idx: usize, new_refs: Vec<Ref>) {
        let mut concat_refs = Vec::with_capacity(new_refs.len() + self.vertices[idx].refs.len());

        for item_ref in new_refs {
            if let Some(id) = item_ref.doi {
                let new_idx = self.get_or_add_key(id);
                concat_refs.push(new_idx as u32);
                self.vertices[new_idx].refs.push(idx as u32);
            }
        }

        self.vertices[idx].ref_by = concat_refs.len() as u16;
        self.push_outgoing_refs(idx, concat_refs);
    }
}

impl Drop for Graph {
    fn drop(&mut self) {
        self.dois.keys().for_each(|&key| key.drop_string());
    }
}
