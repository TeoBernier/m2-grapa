pub mod graph;
pub mod serialize;

mod inner {
    pub use crate::graph::*;
    pub use crate::serialize::*;
    pub use serde::{Deserialize, Serialize};
    pub use std::collections::HashMap;
}

pub mod prelude {
    pub use crate::graph::*;
    pub use crate::serialize::*;
}


use pyo3::prelude::*;
use graph::py_api::ExtractGraph;

#[pymodule]
fn extract(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<ExtractGraph>()?;
    Ok(())
}